import csv
import math
from sklearn import model_selection
from typing import Tuple




class RecommendationManager :

    def __init__(self,data_file_name,random_state: float = 1,test_size = 0.1):
        #self.load_data(data_file_name)

        #DATA = constant value
        self.DATA = self.load_data(data_file_name)
        self.TRAINING_DATA,self.TESTING_DATA = self.split_training_and_testing_data(self.DATA,
        random_state,test_size)
        
        
    #def load_data(self,data_file_name):
    #change method to class method => do not need to parse self
    @staticmethod
    def load_data(data_file_name):
        # instead of printing each row ==> append each row into list
        # ex : ['frozen vegetables', 'mineral water', 'carrots', 'french fries']
        #       ['turkey', 'melons']
        # data <= a list
        data = []

        with open(data_file_name) as data_file:
            # create instance of csv reader
            csv_reader = csv.reader(data_file)
            for row in csv_reader :
                data.append(row)
            
            return data

    @staticmethod
    def split_training_and_testing_data(data,random_state : float, test_size: float) -> Tuple[list,list]:
        #random_state = random seed ทำให้ผลลัพธ์ของการ random ออกมาเหมือนเดิมทุกครั้ง
        training_data, testing_data = model_selection.train_test_split(data,
        random_state = random_state, test_size = test_size)

        return (training_data, testing_data)

if __name__ == "__main__" :
     recommendation_manager = RecommendationManager("store_data.csv")
     print(recommendation_manager.DATA)