import pandas
import argparse
from apyori import apriori
from sklearn import model_selection

def load_data(filename):
    data = pandas.read_csv(filename)
    return data

def preprocess(data):
    item_list = data.unstack().dropna().unique()
    train, test = model_selection.train_test_split(data, test_size=.10)

    train = train.T.apply(lambda x: x.dropna().tolist()).tolist()
    test = test.T.apply(lambda x: x.dropna().tolist()).tolist()
    test_x_y = []
    for i in test:
        if len(i) > 1:
            test_x_y.append((i[:-1], i[-1]))
    return train, test_x_y

def model(train_data, min_support=0.0045, min_confidence=0.2, min_lift=3, min_length=2):
    result = list(apriori(train_data, min_support=min_support, min_confidence=min_confidence, min_lift=min_lift, min_length=min_length))
    return result

def visualize(result):
    for rule in result:
        pair = rule[0]
        components = [i for i in pair]
        print(pair)
        print('Rule:', components[0], '->', components[1])
        print('Support:', rule[1])
        print('Confidence:', rule[2][0][2])
        print('Lift:', rule[2][0][3])
        print('+++++++++++++++++++++++++++++++++')

def argument():
    parser = argparse.ArgumentParser()
    parser.add_argument('data', type=str, help="csv file as a dataset")
    parser.add_argument('--min_length', '-m', type=int, default=3, help="minimum number of products.")
    return parser

def eval(apriori_rule, test):
    for x, y in test:
        candidates = []
    for rule in apriori_rule:
        all_item_in_rule = True
        for item in x:
            if item not in rule[0]:
                all_item_in_rule = False
                break
        if all_item_in_rule:
            candidates.append(rule)
    best_candidate = [[]]
    best_candidate_value = 0
    for candidate in candidates:
        if candidate[2][0][3] > best_candidate_value:
            best_candidate_value = candidate[2][0][3]
            best_candidate = candidate
    if y in best_candidate[0]:
        print(best_candidate[0],y, y in best_candidate[0])

if __name__ == '__main__':
    parser = argument()
    args = parser.parse_args()
    data = load_data(args.data)
    # data_2 = load_data('store2_data.csv')
    train, test = preprocess(data)
    result = model(train, min_length=args.min_length)
    visualize(result)